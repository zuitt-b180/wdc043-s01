package con.zuitt;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scannerName = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = scannerName.nextLine();

        System.out.println("Last Name:");
        String lastName = scannerName.nextLine();

        System.out.println("First Subject Grade:");
        Double firstSubject = scannerName.nextDouble();

        System.out.println("Second Subject Grade:");
        Double secondSubject = scannerName.nextDouble();

        System.out.println("Third Subject Grade:");
        Double thirdSubject = scannerName.nextDouble();

        Double average = ((firstSubject + secondSubject + thirdSubject) / 3);

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + average);
    }
}
