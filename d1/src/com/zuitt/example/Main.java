package com.zuitt.example;
import java.util.Scanner;

public class Main {

    /*
        Main Class
            The Main class is the entry point of our Java Program. It is responsible for executing our code. Every Java program should have at least 1 class and 1 function inside it.

    * */

    public static void main(String[] args) {


        /*
            - main function is where most executable code is applied to.
            - You can create an intellij Java project with a template that already uses or has a main class and function.

            - the "public" keyword is what we call an "access modifier"; it tells our application what parts of the program can access the main function

            - the "void" is the return statement's data type of the "main" function that it defines what kind of data will be returned. "void" means that the main function will return nothing.

        */

        // System.out.println() - is a statement which will allow us to print the value of the argument passed into it.

        System.out.println("Hello World!");

        // Variables and data types
            // in java, to be able to declare a variable, we have to identify or declare its data type. Which means, that the variable will expect and only accept data with the type declared

        int myNum;
        myNum = 3;

        System.out.println(myNum);
        // myNum = "sample string" (error)
        myNum = 50;

        // type "sout" - shortcut
        System.out.println(myNum);

        // byte = -128 to 127
        byte students = (byte) 128;
        System.out.println(students);

        // short = -32,768 to 32,767
        short seats = 32767;
        System.out.println(seats);

        int localPopulation = 2147483647;
        System.out.println(localPopulation);

        // L is added at the end of the long number to recognized as long. if that is omitted, it will wrongfully recongnized as int.
        long worldPopulation = 7862881145L;
        System.out.println(worldPopulation);

        // float can only have 7 decimal places and the final decimal place is being rounded off
        // have a tendency to have wrong rounding off
        float piFloat = 3.1415926f;
        System.out.println(piFloat);

        // much more precise and accurate
        double piDouble = 3.1415926;
        System.out.println(piDouble);

        // char - can only hold 1 character
        // single quotes is for char literal
        // double quotes is for string literals
        char letter = 'a';
        System.out.println(letter);

        // boolean
        boolean isMVP = true;
        System.out.println(isMVP);

        boolean isChampion = false;
        System.out.println(isChampion);

        // final - creates a constant variable - same as in JS, it cannot be re-assigned.
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        // String - in Java, is non-primitive data type. Because in java, Strings are objects that can use methods.
        // Non-primitive data types have access to methods.
            // examples are: array, class, interface
        String username = "chesterSmith29";
        System.out.println(username);

        System.out.println(username.isEmpty());
        // .isEmpty() is a method of a string which returns a boolean. It will check the length of the string and return true, if the length is "zero". It will return false, if otherwise.
            // reassignment of variable
            username = "";
        System.out.println(username.isEmpty());

        // Scanner - is a class which allows us to input into our program, it's kind of similar to prompt() in JS. However, Scanner, being a class, has to be imported to be used.
        Scanner scannerName = new Scanner(System.in);

        // .nextLIne() allows us to accept and store the next string by our user's input.
        System.out.println("What is our name?");
        String myName = scannerName.nextLine();

                System.out.println("Your name is " + myName + "!");

        // nextInt() allows us to accept and store the next integer by our user's input.
        System.out.println("What is your age?");
        int myAge = scannerName.nextInt();
        // if we use 'String' here instead of 'int', ERROR will occur

                System.out.println("You are " + myAge + " years old!");

        System.out.println("What is your first quarter grade?");
        double firstQuarter = scannerName.nextDouble();

                System.out.println("Your first quarter grade is " + firstQuarter + "!");

        System.out.println("Enter a number:");
        int number1 = scannerName.nextInt();
        System.out.println("Enter a number:");
        int number2 = scannerName.nextInt();

        // just like in JS, we can use + - * / for mathematical operations
        int sum = number1 + number2;
        System.out.println("The sum of the numbers are: " + sum);

        /*
            Mini-Activity

            1. Add a system out print line to ask the user for a number. Create a new integer variable and store the value returned by using the scanner and its nextInt() method.
            2. Add a system out print line to ask the user for a number. Create a new integer variable and store the value returned by using the scanner and its nextInt() method.
            3. Create a new integer variable to store the difference of the numbers that was input.
            4. Print the value of the difference between the input numbers.
        */

        System.out.println("Enter a number:");
        int firstNum = scannerName.nextInt();
        System.out.println("Enter a number:");
        int secondNum = scannerName.nextInt();

        // just like in JS, we can use + - * / for mathematical operations
        int difference = firstNum - secondNum;
        System.out.println("The difference of the numbers are: " + difference);



    }

}
